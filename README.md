# docker-compose-example
Example of docker compose

## Run app

```
docker-compose up -d
```

## Scale app

```
docker-compose scale celery=10
```

## Stop app

```
docker-compose stop
```

## Remove containers

```
docker-compose rm -f
```